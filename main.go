/*
 * Copyright 2018 Koki Fukuda
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package main

import (
	"bufio"
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/elazarl/goproxy"
	_ "github.com/mattn/go-sqlite3"
)

var forbiddenTemplate = template.Must(template.New("").Parse(`<!doctype html>
<meta name="viewport" content="width=device-width">
<title>You are Blocked!</title>
<body style="font-family: sans-serif;">
<h1>You are Blocked!</h1>
<p>Connection to {{ .host }} has been blocked by Seeed HTTP Proxy</p>
<p>To allow connection to this host, visit administration page or run <code>seeed delete {{ .host }}</code> on server's terminal.`))

var portRegexp = regexp.MustCompile(":[0-9]{1,5}$")

func stripPort(host string) string {
	return portRegexp.ReplaceAllString(host, "")
}

func allowed(host string, db *sql.DB) bool {
	hostname := stripPort(host)
	for _, h := range split(hostname) {
		row := db.QueryRow("SELECT host FROM reject WHERE host = ? LIMIT 1", h)
		var found string
		err := row.Scan(&found)
		isInDb := err == nil
		if isInDb {
			return false
		}
	}
	return true
}

func importHosts(filename string, db *sql.DB) {
	var f *os.File
	if filename == "-" {
		f = os.Stdin
	} else {
		var err error
		f, err = os.Open(filename)
		if err != nil {
			return
		}
		defer f.Close()
	}
	r := bufio.NewReader(f)
	for {
		b, _, err := r.ReadLine()
		if err != nil {
			break
		}
		if len(b) != 0 && b[0] != '#' {
			host := string(b)
			_, err = db.Exec("INSERT INTO reject VALUES(?)", host)
			if err == nil {
				fmt.Println(host)
			} else {
				printlnRed(os.Stdout, host)
			}
		}
	}
}

func dumpDatabase(db *sql.DB) {
	rows, err := db.Query("SELECT host FROM reject ORDER BY host ASC")
	if err != nil {
		log.Fatal("Failed to collect data.")
	}
	defer rows.Close()
	for rows.Next() {
		var host string
		if err = rows.Scan(&host); err != nil {
			log.Fatal("Error occurred while loading data.")
		}
		fmt.Println(host)
	}
}

var domainPrefixes = []string{}

func add(db *sql.DB, host string, force bool) {
	if host == "" {
		return
	}
	if !force {
		var eh string
		for _, h := range split(host) {
			row := db.QueryRow("SELECT host FROM reject WHERE host = ?", h)
			err := row.Scan(&eh)
			if err == nil {
				printlnRed(os.Stderr, host+"(Covered by "+eh+")")
				return
			}
		}
		rows, err := db.Query("SELECT host FROM reject WHERE host LIKE ?", "_%."+host)
		if err != nil {
			log.Fatal(err)
		}
		var willDelete string
		for rows.Next() {
			err = rows.Scan(&willDelete)
			if err != nil {
				log.Fatal(err)
			}
			printlnRed(os.Stderr, willDelete+" will be deleted.")
		}
		db.Exec("DELETE FROM reject WHERE host LIKE ?", "_%."+host)
	}
	_, err := db.Exec("INSERT INTO reject VALUES(?)", host)
	if err == nil {
		fmt.Println(host)
	} else {
		printlnRed(os.Stderr, host)
	}
}

func addHostOf(db *sql.DB, rawUrl string, force bool) {
	parsedUrl, err := url.Parse(rawUrl)
	if err != nil {
		printlnRed(os.Stderr, rawUrl)
		return
	}
	host := stripPort(parsedUrl.Host)
	add(db, host, force)
}

func deleteHost(db *sql.DB, host string) {
	_, err := db.Exec("DELETE FROM reject WHERE host = ?", host)
	if err == nil {
		fmt.Println(host)
	} else {
		printlnRed(os.Stderr, host)
	}
}

func vacuum(db *sql.DB) {
	db.Exec("VACUUM")
}

func prepareDatabase() *sql.DB {
	dbfile := filepath.Join(getConfDir(), "reject.db")
	db, err := sql.Open("sqlite3", dbfile)
	if err != nil {
		log.Fatal("Cannot open database.", err)
	}
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS reject(host VARCHAR(32) UNIQUE NOT NULL)`)
	if err != nil {
		log.Fatal("Cannot initialize database.", err)
	}
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS shorten(name VARCHAR(32) UNIQUE NOT NULL, url VARCHAR(1024))`)
	if err != nil {
		log.Fatal("Cannot initialize database")
	}
	return db
}

func getConfDir() string {
	executable, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	dir := filepath.Dir(executable)
	confDir := filepath.Join(dir, "conf")
	if _, err := os.Stat(confDir); os.IsNotExist(err) {
		err = os.Mkdir(confDir, os.ModePerm)
		if err != nil {
			log.Fatal(err)
		}
	}
	return confDir
}

func getPidFileName() string {
	return filepath.Join(getConfDir(), "seeed.pid")
}

var forceStart = false

func createPidFile() {
	pidfilename := getPidFileName()
	if _, err := os.Stat(pidfilename); !os.IsNotExist(err) {
		if !forceStart {
			log.Fatal("Server is already running.")
		}
		fmt.Fprintln(os.Stderr, "Warning: Trying to start in spite of pid file existing.")
		fmt.Fprintln(os.Stderr, "Warning: This may fail and disturb normal server's boot up later.")
		os.Remove(pidfilename)
	}
	file, err := os.Create(pidfilename)
	if err != nil {
		log.Fatal("Cannot create pid file; proxy may have already been running.", err)
	}
	defer file.Close()
	r := bufio.NewWriter(file)
	r.Write([]byte(strconv.Itoa(os.Getpid())))
	r.Flush()
}

func tearDown() {
	os.Remove(getPidFileName())
}

func getServerPid() (int, error) {
	if _, err := os.Stat(getPidFileName()); os.IsNotExist(err) {
		return 0, err
	}
	file, err := os.Open(getPidFileName())
	if err != nil {
		return 0, err
	}
	defer file.Close()
	r := bufio.NewReader(file)
	b, _, err := r.ReadLine()
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(string(b))
}

func kill() {
	pid, err := getServerPid()
	if err != nil {
		log.Fatal("Unable to load pid")
	}
	proc, err := os.FindProcess(pid)
	if err != nil {
		log.Fatal("Process pointed by the pid file not running.")
	}
	proc.Signal(os.Interrupt)
	proc.Wait()
}

var daemonized = false

func createDenyResp(host string) string {
	buf := &bytes.Buffer{}
	templArgs := map[string]interface{}{
		"host":   host,
		"daemon": daemonized,
	}
	forbiddenTemplate.Execute(buf, templArgs)
	return buf.String()
}

func addShorten(db *sql.DB, name, url string) {
	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		url = "http://"+url
	}
	_, err := db.Exec(`INSERT INTO shorten VALUES(?, ?)`, name, url)
	if err != nil {
		log.Fatal("Cannot add shortened url")
	}
}

func createShortenerResponse(db *sql.DB, r *http.Request, name string) (*http.Response, error) {
	if strings.HasPrefix(name, "/") {
		name = strings.TrimPrefix(name, "/")
	}
	row := db.QueryRow("SELECT url FROM shorten WHERE name = ? LIMIT 1", name)
	var url string
	err := row.Scan(&url)
	if err != nil {
		return nil, err
	}
	result := goproxy.NewResponse(r, goproxy.ContentTypeHtml, 302, "")
	result.Header.Set("Location", url)
	return result, nil
}

func runProxy(db *sql.DB) {
	proxy := goproxy.NewProxyHttpServer()
	proxy.OnRequest().DoFunc(
		func(r *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
			host := r.URL.Host
			if host == "go" {
				if verbose {
					fmt.Println("shortener:", r.URL.Path)
				}
				resp, err := createShortenerResponse(db, r, r.URL.Path)
				if err != nil {
					return r, nil
				} else {
					return r, resp
				}
			}
			if allowed(host, db) {
				if verbose {
					fmt.Println(host)
				}
				return r, nil
			} else {
				return r, goproxy.NewResponse(r, goproxy.ContentTypeHtml,
					http.StatusForbidden, createDenyResp(host))
			}
		})
	proxy.OnRequest().HandleConnectFunc(
		func(host string, ctx *goproxy.ProxyCtx) (*goproxy.ConnectAction, string) {
			if allowed(host, db) {
				if verbose {
					fmt.Println(host)
				}
				return goproxy.OkConnect, host
			} else {
				return goproxy.RejectConnect, host
			}
		})
	log.Fatal(http.ListenAndServe(":8000", proxy))
}

func printStatus() {
	pid, err := getServerPid()
	if err != nil {
		fmt.Println("Server is not active.")
		return
	}
	fmt.Println("Server is running. Pid:", pid)
}

func printVersion() {
	fmt.Fprintln(os.Stdout, "Seeed HTTP Proxy 1.2")
}

func printUsageAndExit(exitCode int) {
	printVersion()
	fmt.Fprintln(os.Stdout, `Usage: seeed VERB [OPTIONS...]

Seeed HTTP Proxy is a HTTP proxy with block functionally based on host name.

Verbs and Options:
run          Starts server until interrupted.
    -v --verbose          Prints accessing host.
    -d --daemon           Execute server as daemon.
    -h --hot              Restart internally.
    -f --force            Try to start even if pid file exists.
import       Imports hostname from given file(s).
add          Adds given host(s).
    -f --force            Add host which current database covers.
add-host-of  Add host of specified URL.
    -f --force            Add host which current database covers.
rm           Delets given host(s). Can't be take more than one option.
    --vacuum              Run VACUUM on internal SQLite database.
kill         Kills currently running server.
status       Prints whether running or not and if running, pid.
add-shorten  [name] [url]
dump         Prints all registered hosts.`)
	os.Exit(exitCode)
}

func killSilentlyIfNeeded() {
	if pid, err := getServerPid(); err == nil {
		proc, err := os.FindProcess(pid)
		if err == nil {
			proc.Signal(os.Interrupt)
			proc.Wait()
		}
	}
}

var verbose = false
var daemon = false

func handleOption(db *sql.DB) (start bool) {
	if len(os.Args) <= 1 {
		printUsageAndExit(1)
	}
	if os.Args[1] == "--help" {
		printUsageAndExit(0)
	} else if os.Args[1] == "--version" {
		printVersion()
		os.Exit(0)
	}
	subCommand := os.Args[1]
	switch subCommand {
	case "run":
		for _, f := range os.Args[2:] {
			if strings.HasPrefix(f, "--") {
				switch f {
				case "--verbose":
					verbose = true
				case "--daemon":
					daemon = true
				case "--hot":
					killSilentlyIfNeeded()
				case "--force":
					forceStart = true;
				case "--daemonized-proccess":
					daemonized = true
				default:
					printUsageAndExit(1)
				}
			} else if strings.HasPrefix(f, "-") {
				if len(f) == 1 {
					printUsageAndExit(1)
				}
				for _, sf := range f[1:] {
					switch sf {
					case 'v':
						verbose = true
					case 'd':
						daemon = true
					case 'h':
						killSilentlyIfNeeded()
					case 'f':
						forceStart = true
					default:
						printUsageAndExit(1)
					}
				}
			} else {
				printUsageAndExit(1)
			}
		}
		return true
	case "import":
		for _, f := range os.Args[2:] {
			importHosts(f, db)
		}
	case "add":
		forceAdd := false
		for _, h := range os.Args[2:] {
			if h == "-f" || h == "--force" {
				forceAdd = true
				continue
			} else if strings.HasPrefix(h, "-") {
				fmt.Fprintln(os.Stderr, "Unrecognized option:", h)
				printUsageAndExit(1)
			}
			add(db, h, forceAdd)
		}
	case "add-host-of":
		forceAdd := false
		for _, h := range os.Args[2:] {
			if h == "-f" || h == "--force" {
				forceAdd = true
				continue
			} else if strings.HasPrefix(h, "-") {
				fmt.Fprintln(os.Stderr, "Unrecognized option:", h)
				printUsageAndExit(1)
			}
			addHostOf(db, h, forceAdd)
		}
	case "rm":
		if len(os.Args) >= 3 && strings.HasPrefix(os.Args[2], "-") {
			if len(os.Args) > 3 {
				printUsageAndExit(1)
			}
			switch os.Args[2] {
			case "--vacuum":
				vacuum(db)
			default:
				printUsageAndExit(1)
			}
			os.Exit(0)
		}
		for _, h := range os.Args[2:] {
			deleteHost(db, h)
		}
	case "kill":
		kill()
	case "status":
		printStatus()
	case "add-shorten":
		if len(os.Args) == 4 {
			addShorten(db, os.Args[2], os.Args[3])
		} else {
			fmt.Fprintln(os.Stderr, "Usage: seeed add-shorten [name] [url]")
			os.Exit(1)
		}
	case "dump":
		dumpDatabase(db)
	default:
		printUsageAndExit(1)
	}
	return false
}

func main() {
	db := prepareDatabase()
	if !handleOption(db) {
		return
	}
	if daemon {
		executable, err := os.Executable()
		if err != nil {
			log.Fatal(err)
		}
		args := []string{"run", "--daemonized-proccess"}
		cmd := exec.Command(executable, args...)
		cmd.Start()
		os.Exit(0)
	}
	createPidFile()
	defer tearDown()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		close(c)
		tearDown()
		os.Exit(0)
	}()
	runProxy(db)
}
